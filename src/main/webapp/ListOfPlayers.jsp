<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <title>Players list</title>
</head>
<body>
<div class="w3-container">
    <h1 style="text-align:center">Players list</h1>

    <h2 style="text-align:center">Hello, ${newPlayer}!</h2>

    <h2 style="text-align:center">See the list of registered players, and, maybe, your death enemies!</h2>
</div>
<div class="w3-container" style="text-align:left">
    <ul>
        <c:forEach  items="${listOfPlayers}" var="player">
            <li style="font-size:150%">  <c:out value="${player.login}  ${player.fullName}"/></li>
        </c:forEach>
    </ul>
</div>

<div class="w3-container">
    <a href="/rock-paper-scissors/List?name=${newPlayer}"><button class="w3-btn">See game list</button></a>
</div>

</body>
</html>