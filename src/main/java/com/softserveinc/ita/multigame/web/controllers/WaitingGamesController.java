package com.softserveinc.ita.multigame.web.controllers;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameListManager;
import java.io.IOException;

@WebServlet("/Waiting/*")
public class WaitingGamesController extends HttpServlet  {


	private static final long serialVersionUID = 9038632495101562362L;

	GameListManager gameListManagerService = GameListManager.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Long id = Long.parseLong(request.getParameter("id"));
		String enemy = gameListManagerService.getGameById(id).getFirstPlayer().getLogin();

		request.setAttribute("id", request.getParameter("id"));
		request.setAttribute("name", request.getParameter("name"));
		request.setAttribute("enemy", enemy);

		ServletContext ctx = getServletContext();
		RequestDispatcher dispatcher = ctx.getRequestDispatcher("/WaitingGames.jsp");
		dispatcher.forward(request, response);
	}

}
