package com.softserveinc.ita.multigame.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

	@WebServlet("/Dispatcher")
   public class Dispatcher extends HttpServlet  {

	private static final long serialVersionUID = -3304454848870544677L;

	protected void forward(String address, HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }
   }

