package com.softserveinc.ita.multigame.web.controllers;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.engine.RockScissorsPaper;

import java.io.IOException;

@WebServlet("/MakeTurn")
public class MakeTurnController extends HttpServlet {

	private static final long serialVersionUID = -6063483204961583507L;

	GameListManager gameManagerService = GameListManager.getInstance();
	RockScissorsPaper rsp = null;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String turn = request.getParameter("turn");
		String currentPlayerName = request.getParameter("currentPlayerName");
		Long id = Long.parseLong(request.getParameter("id"));

		rsp = (RockScissorsPaper) gameManagerService.getGameById(id).getGameEngine();

		String enemy = null;
		if (gameManagerService.getGameById(id).getFirstPlayer().getLogin().equals(currentPlayerName)) {
			enemy = gameManagerService.getGameById(id).getSecondPlayer().getLogin();
		} else {
			enemy = gameManagerService.getGameById(id).getFirstPlayer().getLogin();
		}

			/*Starting our game*/
		rsp.setFirstPlayer(currentPlayerName);
		rsp.setSecondPlayer(enemy);
		rsp.isStarted();
		gameManagerService.getGameById(id).getGameEngine().makeTurn(currentPlayerName, turn);

		boolean isStarted = gameManagerService.getGameById(id).getGameEngine().isStarted();
		String winner =  gameManagerService.getGameById(id).getGameEngine().getTheWinner();
		int resultCode = gameManagerService.getGameById(id).getGameEngine().getResultCode();
		String gameState = gameManagerService.getGameById(id).getGameEngine().toString();

		request.setAttribute("gameState", gameState);
		request.setAttribute("currentPlayerName", currentPlayerName);
		request.setAttribute("enemy", enemy);
		request.setAttribute("id", id);
		request.setAttribute("winner", winner);
		request.setAttribute("resultCode", resultCode);
		request.setAttribute("isStarted", isStarted);

		ServletContext ctx = getServletContext();
		RequestDispatcher dispatcher = ctx.getRequestDispatcher("/PlayingGames.jsp");
		dispatcher.forward(request, response);

	}


}