package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayerListManager;


@WebServlet("/List")
public class GameListController extends HttpServlet {

	private static final long serialVersionUID = 3162561535639239330L;

	PlayerListManager playerManagerService =  PlayerListManager.getInstance();
	GameListManager gameListManagerService = GameListManager.getInstance();
	Player currentPlayer = null;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String currentPlayerName = request.getParameter("name");
		currentPlayer = playerManagerService.getPlayerByName(currentPlayerName);

		List<Long> listOfCreatedGames = gameListManagerService.getCreatedGamesIds(currentPlayer);
		List<Long> listOfWaitingGames = gameListManagerService.getWaitingGamesIds(currentPlayer);
		List<Long> listOfPlayingGames = gameListManagerService.getPlayingGamesIds(currentPlayer);

		request.setAttribute("currentPlayerName", currentPlayerName);
		request.setAttribute("listOfCreatedGames", listOfCreatedGames);
		request.setAttribute("listOfWaitingGames", listOfWaitingGames);
		request.setAttribute("listOfPlayingGames", listOfPlayingGames);

		getServletContext().getRequestDispatcher("/ListOfGames.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String currentPlayerName = request.getParameter("name");
		currentPlayer = playerManagerService.getPlayerByName(currentPlayerName);

		request.setAttribute("currentPlayerName", currentPlayerName);
		if (currentPlayer == null) {
			getServletContext().getRequestDispatcher("/registration.html").forward(request, response);
		} else
			doGet(request, response);
	}

}