package com.softserveinc.ita.multigame.web.controllers;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayerListManager;



@WebServlet("/AddPlayer")
public class AddPlayerController extends  HttpServlet {

	private static final long serialVersionUID = 4054414597069364373L;

	Player newPlayer = null;
	PlayerListManager playerManagerService = PlayerListManager.getInstance();


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("save") != null ) {

			String newName = request.getParameter("name");
			String newFullName = request.getParameter("playerFullName");

			newPlayer = new Player(newName, newFullName);

			boolean result = playerManagerService.registerNewPlayer(newPlayer);
			request.setAttribute("newPlayer", newPlayer.getLogin());
			request.setAttribute("listOfPlayers", playerManagerService.getAllPlayers());


			if (result) {

				getServletContext().getRequestDispatcher("/ListOfPlayers.jsp").forward(request, response);
			} else {

				getServletContext().getRequestDispatcher("/errorRegistration.html").forward(request, response);
			}

		} else if (request.getParameter("cancel") != null){
			getServletContext().getRequestDispatcher("/login.html").forward(request, response);
		}

	}

}

