package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

public class Game {
	private Player firstPlayer;
	private Player secondPlayer;
	private GenericGameEngine<String> gameEngine;
	private Long id;
	
	public Game() {
		super();
	}
	
	public Game(Player firstPlayer,  GenericGameEngine<String> gameEngine) {
		this.firstPlayer = firstPlayer;
		this.gameEngine = gameEngine;
		gameEngine.setFirstPlayer(firstPlayer.getLogin());
		this.setId(gameEngine.getId());	
	}
	
	public Game(Player firstPlayer, Player secondPlayer, GenericGameEngine<String> gameEngine) {
		this.firstPlayer = firstPlayer;
		this.secondPlayer = secondPlayer;
		this.gameEngine = gameEngine;
		gameEngine.setFirstPlayer(firstPlayer.getLogin());
		gameEngine.setSecondPlayer(secondPlayer.getLogin());
		this.setId(gameEngine.getId());
	}
	
	
	public Player getFirstPlayer() {
		return firstPlayer;
	}
	
	public void setFirstPlayer(Player firstPlayer) {
		this.firstPlayer = firstPlayer;
	}
	
	public Player getSecondPlayer() {
		return secondPlayer;
	}
	
	public void setSecondPlayer(Player secondPlayer) {
		this.secondPlayer = secondPlayer;
	}
	
	public GenericGameEngine<String> getGameEngine() {
		return gameEngine;
	}
	
	public void setGameEngine(GenericGameEngine<String> gameEngine) {
		this.gameEngine = gameEngine;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gameEngine == null) ? 0 : gameEngine.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (gameEngine == null) {
			if (other.gameEngine != null)
				return false;
		} else if (!gameEngine.equals(other.gameEngine))
			return false;
		return true;
	}
	
	

}
