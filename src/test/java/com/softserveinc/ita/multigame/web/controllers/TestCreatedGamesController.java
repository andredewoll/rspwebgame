package com.softserveinc.ita.multigame.web.controllers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TestCreatedGamesController extends Mockito {
	 @Mock
	 private HttpServletRequest request; 
	 @Mock
	 private HttpServletResponse response; 
	 @Mock
	 private RequestDispatcher dispatcher; 
	 @Mock
	 private ServletContext ctx;
	 
	 private CreatedGamesController controller;
	 
	 @Before
	 public void setUp() throws Exception {
	  MockitoAnnotations.initMocks(this);
	  MockitoAnnotations.initMocks(ctx);
	  
	  controller = new CreatedGamesController();
	 }
	 
	 @Test(expected = IllegalStateException.class)
	 public void test() throws ServletException, IOException {
		  when(request.getParameter("id")).thenReturn("id");
		  when(request.getParameter("name")).thenReturn("name");
		  when(request.getRequestDispatcher("/CreatedGames.jsp")).thenReturn(dispatcher);
		  controller.doGet(request, response);
		  
		  verify(request).getParameter("id");
		  verify(request).getParameter("name");
	 }
	  

}
