package com.softserveinc.ita.multigame.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TurnServiceTest {
	
	private TurnService service;
	private Player player;
	private GameListManager gameListManager;
	
	@Before
	public void setup() {
		gameListManager = GameListManager.getInstance();
		player = mock(Player.class, Mockito.CALLS_REAL_METHODS);
		player.setLogin("vasya");
		when(player.getLogin()).thenReturn("vasya");
	}
	
	@Test(expected = NullPointerException.class)
	public void testingMakeTurnValidationWithRules() {
		assertTrue(gameListManager.getGameById(1L).getGameEngine().makeTurn(player.getLogin(), "ROCK"));
	}
	
	@Test(expected = NullPointerException.class)
	public void testingMakeTurnValidationWithWrongRules() {
		assertFalse(gameListManager.getGameById(1L).getGameEngine().makeTurn(player.getLogin(), "RK"));
	}

}
