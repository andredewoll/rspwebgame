package com.softserveinc.ita.multigame.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class PlayerListManagerTest {
	
	private PlayerListManager manager;
    private static final Player player1 = new Player("1");
    private static final Player player2 = new Player("2");
    private static final Player player3 = new Player("3");
    private static final Player player4 = new Player("4");
    
    @Before
    public void setUp() {
	manager = PlayerListManager.getInstance();
    }
    
    @After
    public void tireDown() {
    	try {
    	    Field field = PlayerListManager.class.getDeclaredField("instance");
    	    field.setAccessible(true);
    	    field.set(null, null);
    	} catch (Exception e) {
    	    fail("Field \"manager\" is not accessible");
    	}
    	try {
    	    Field field = PlayerListManager.class.getDeclaredField("map");
    	    field.setAccessible(true);
    	    field.set(null, null);
    	} catch (Exception e) {
    	    fail("Field \"map\" is not accessible");
    	}
    	
        }
    
    @Test
    public void newCreatedManagerShouldBeEmpty() {
	assertEquals(0, manager.getAllPlayers().size());
    }
    
    @Test
    public void managerIsSingleton() {
	PlayerListManager newmanager = PlayerListManager.getInstance();
	assertSame(newmanager, manager);
    }
    
    @Test
    public void managerContainsAsManyPlayersAsHaveBeenAdded() {
	fillManagerWithPlayers();
	assertEquals(4, manager.getAllPlayers().size());
    }
    
    @Test
    public void ifManagersMapIsClearedItHasToBeEmpty() {
    fillManagerWithPlayers();
	manager.clear();
	assertEquals(0, manager.getAllPlayers().size());
    }
    
    @Test
    public void ifManagerContainsSomeObjectShouldBeTrue() {
    	fillManagerWithPlayers();
    	assertTrue( manager.contains(player1));
    }
    
    @Test 
    public void managerWillGiveYouPlayerByName() {
    	fillManagerWithPlayers();    	
    	assertSame(player1, manager.getPlayerByName("1"));
    }
    
    @Test
    public void firstSavedPlayerCouldBeGetFromTheManager() {
    fillManagerWithPlayers();
	Player player = manager.getPlayerByName(player1.getLogin());
	assertEquals(player1, player);
    }
    
    @Test
    public void lastSavedPlayerCouldBeGetFromTheManager() {
    	fillManagerWithPlayers();
	Player player = manager.getPlayerByName(player4.getLogin());
	assertEquals(player4, player);
    }
    
    @Test
    public void playerShouldNotBeSavedTwiceInManager() {
    fillManagerWithPlayers();
	assertFalse(manager.registerNewPlayer(player1));
	assertEquals(4, manager.getAllPlayers().size());
    }
    
    
    
    
    private void fillManagerWithPlayers() {
    	manager.registerNewPlayer(player1);
    	manager.registerNewPlayer(player2);
    	manager.registerNewPlayer(player3);	
    	manager.registerNewPlayer(player4);
        }

}
